<?php

/* ************************************************************************************************************************

Surtiacrylicos

File:      index.php
Author:      Amapola Azul
Homepage:    www.amapolazul.com
Copyright:    2013

************************************************************************************************************************ */

defined( '_JEXEC' ) or die( 'Acceso Restringido.' );

JHtml::_( 'behavior.framework', true );

// Variables

$site_base = $_SERVER['HTTP_HOST']; // e.g. www.amapolazul.com
$site_path = 'http://' . $site_base; // e.g. http://www.amapolazul.com
$app = JFactory::getApplication();
$option = JRequest::getVar( 'option' );
$view = JRequest::getVar( 'view' );
$Itemid = JRequest::getVar( 'Itemid' );

// Template path

$path = 'templates/' . $this->template . '/';

// Modules

$show_banners = $this->countModules( 'banners' );

// Params

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php echo $this->language; ?>" xml:lang="<?php echo $this->language; ?>">
<head>
<jdoc:include type="head" />
<!-- Begin Open Graph Protocol -->
<meta property="og:url" content="<?php echo $site_path; ?>" />
<meta property="og:type" content="company" />
<meta property="og:title" content="<?php echo $app->getCfg( 'sitename' ); ?>" />
<meta property="og:description" content="<?php echo $app->getCfg( 'MetaDesc' ); ?>" />
<meta property="og:image" content="<?php echo $site_path; ?>/<?php echo $path; ?>images/logo_ogp.png" />
<link rel="image_src" href="<?php echo $site_path; ?>/<?php echo $path; ?>images/logo_link_ogp.png" />
<!-- End Open Graph Protocol -->
<link href="css/template.css" rel="stylesheet" type="text/css" media="screen" />
<link href="<?php echo $path; ?>css/template.css" rel="stylesheet" type="text/css" media="screen" />
<!-- Begin Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
 
  ga('create', 'UA-39034949-1', 'auto');
  ga('send', 'pageview');
 
</script>
<!-- End Google Analytics -->
</head>
<body>
<!-- Begin All -->
  <div class="all">
    <!-- Begin Top -->
      <div class="top">
        <jdoc:include type="modules" name="top" style="xhtml" />
        <div class="clear"></div>
      </div>
    <!-- End Top -->
    <!-- Begin Separator -->
      <div class="separator"></div>
    <!-- End Separator -->
    <!-- Begin Left -->
      <div class="left">
        <jdoc:include type="modules" name="left" style="xhtml" />
      </div>
    <!-- End Left -->
    <!-- Begin Component -->
      <div class="component">
        <?php if ( $show_banners ) : ?>
        <!-- Begin Banners -->
          <div class="banners">
            <jdoc:include type="modules" name="banners" style="xhtml" />
            <div class="clear"></div>
          </div>
        <!-- End Banners -->
        <?php else : ?>
        <jdoc:include type="component" />
        <?php endif; ?>
      </div>
    <!-- End Component -->
    <div class="clear"></div>
    <!-- Begin Copyright -->
      <div class="copyright">
        Direcci&oacute;n: Cra 30 bis No. 5b - 55 / Calle 63 No. 22 - 23 Bogot&aacute; - Colombia / Tels. 3 60 5055 - 3 47 0150 - 3 44 1399 / Email: <a href="mailto:ventas@surtiacrylicos.com">ventas@surtiacrylicos.com</a><br />
        &copy; <?php echo date( 'Y' ); ?> <a href="<?php echo $this->baseurl; ?>"><?php echo $app->getCfg( 'sitename' ); ?></a>. Todos los derechos reservados. Sitio desarrollado por <a href="http://www.amapolazul.com" target="_blank">Amapola Azul</a>.
      </div>
    <!-- End Copyright -->
  </div>
<!-- End All -->
</body>
</html>