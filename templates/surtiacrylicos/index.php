<?php

/* ************************************************************************************************************************

Surtiacrylicos

File:			index.php
Author:			Amapolazul Grupo Creativo
Homepage:		www.amapolazul.com
Copyright:		2013

************************************************************************************************************************ */

defined( '_JEXEC' ) or die( 'Acceso Restringido.' );

JHtml::_( 'behavior.framework', true );

// Variables

$site_base = $_SERVER['HTTP_HOST']; // e.g. www.amapolazul.com
$site_path = 'http://' . $site_base; // e.g. http://www.amapolazul.com
$app = JFactory::getApplication();
$option = JRequest::getVar( 'option' );
$view = JRequest::getVar( 'view' );
$Itemid = JRequest::getVar( 'Itemid' );

// Template path

$path = 'templates/' . $this->template . '/';

// Modules

$show_banners = $this->countModules( 'banners' );
$show_banners_mobile = $this->countModules( 'banners_mobile' );

// Params

?>
<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="<?php echo $this->language; ?>"><![endif]-->
<html class="no-js" lang="<?php echo $this->language; ?>">
	<head>
		<jdoc:include type="head" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Begin Open Graph Protocol -->
		<meta property="og:url" content="<?php echo $site_path; ?>">
		<meta property="og:type" content="website">
		<meta property="og:title" content="<?php echo $app->getCfg( 'sitename' ); ?>">
		<meta property="og:description" content="<?php echo $app->getCfg( 'MetaDesc' ); ?>">
		<meta property="og:image" content="<?php echo $site_path; ?>/<?php echo $path; ?>images/logo_ogp.png">
		<link rel="image_src" href="<?php echo $site_path; ?>/<?php echo $path; ?>images/logo_link_ogp.png">
		<!-- End Open Graph Protocol -->
		<link href="<?php echo $path; ?>js/foundation/css/normalize.css" rel="stylesheet">
		<link href="<?php echo $path; ?>js/foundation/css/foundation.min.css" rel="stylesheet">
		<link href="<?php echo $path; ?>js/jquery/fancybox/jquery.fancybox.css?v=2.1.5" rel="stylesheet" type="text/css" media="screen">
		<link href="<?php echo $path; ?>css/font-awesome/css/font-awesome.min.css" rel="stylesheet">
		<link href="<?php echo $path; ?>css/template.css" rel="stylesheet" type="text/css">
		<script src="<?php echo $path; ?>js/foundation/js/vendor/modernizr.js"></script>
		<!-- Begin Google Analytics -->
		<script>

			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-39034949-1', 'surtiacrylicos.com');
			ga('send', 'pageview');

		</script>
		<!-- End Google Analytics -->
	</head>
	<body>
		<!-- Begin All -->
			<div class="all">
				<!-- Begin Top -->
					<div class="top show-for-medium-up">
						<jdoc:include type="modules" name="top" style="xhtml" />
						<div class="clear"></div>
					</div>
					<div class="top_mobile show-for-small-only">
						<div class="row">
							<jdoc:include type="modules" name="top_mobile" style="xhtml" />
						</div>
						<div class="row">
							<nav class="top-bar" data-topbar role="navigation">
								<ul class="title-area">
									<li class="name"></li>
									<li class="toggle-topbar menu-icon"><a href="#"><span>Menú</span></a></li>
								</ul>
								<section class="top-bar-section">
									<jdoc:include type="modules" name="menu" style="xhtml" />
								</section>
							</nav>
						</div>
					</div>
				<!-- End Top -->
				<!-- Begin Separator -->
					<div class="separator"></div>
				<!-- End Separator -->
				<!-- Begin Left -->
					<div class="left show-for-medium-up">
						<jdoc:include type="modules" name="left" style="xhtml" />
					</div>
				<!-- End Left -->
				<!-- Begin Component -->
					<div class="component show-for-medium-up">
						<?php if ( $show_banners ) : ?>
						<!-- Begin Banners -->
							<div class="banners">
								<jdoc:include type="modules" name="banners" style="xhtml" />
								<div class="clear"></div>
							</div>
						<!-- End Banners -->
						<?php else : ?>
						<jdoc:include type="component" />
						<?php endif; ?>
					</div>
				<!-- End Component -->
				<div class="clear"></div>
				<!-- Begin Left & Component -->
					<div class="show-for-small-only">
						<div class="off-canvas-wrap" data-offcanvas>
							<div class="inner-wrap">
								<a href="#" class="left-off-canvas-toggle"><i class="fa fa-bars" aria-hidden="true"></i> Productos</a>
								<aside class="left-off-canvas-menu">
									<!-- Begin Left -->
										<div class="left">
											<jdoc:include type="modules" name="left" style="xhtml" />
										</div>
									<!-- End Left -->
								</aside>
								<!-- Begin Component -->
									<div class="component">
										<?php if ( $show_banners_mobile ) : ?>
										<!-- Begin Banners -->
											<div class="banners">
												<jdoc:include type="modules" name="banners_mobile" style="xhtml" />
												<div class="clear"></div>
											</div>
										<!-- End Banners -->
										<?php else : ?>
										<jdoc:include type="component" />
										<?php endif; ?>
									</div>
								<!-- End Component -->
								<a class="exit-off-canvas"></a>
							</div>
						</div>
					</div>
				<!-- End Left & Component -->
				<!-- Begin Copyright -->
					<div class="copyright">
						Direcci&oacute;n: Cra 30 bis # 5b - 55 / Calle 63 # 22 - 23 Bogot&aacute; - Colombia / Tels. 3 60 5055 - 3 47 0150 - 3 44 1399 / Email: <a href="mailto:ventas@surtiacrylicos.com">ventas@surtiacrylicos.com</a> - <a href="mailto:ventas.63@surtiacrylicos.com">ventas.63@surtiacrylicos.com</a><br />
						&copy; <?php echo date( 'Y' ); ?> <a href="<?php echo $this->baseurl; ?>">Surtiacrylicos</a>. Todos los derechos reservados. Sitio desarrollado por <a href="http://www.amapolazul.com" target="_blank">Amapola Azul</a>.
					</div>
				<!-- End Copyright -->
			</div>
		<!-- End All -->
		<!-- Begin Main Scripts -->
			<script src="<?php echo $path; ?>js/foundation/js/vendor/jquery.js"></script>
			<script src="<?php echo $path; ?>js/foundation/js/foundation.min.js"></script>
			<script src="<?php echo $path; ?>js/jquery/fancybox/jquery.fancybox.pack.js?v=2.1.5" type="text/javascript"></script>
			<script>
				jQuery(document).foundation({
					tooltip: {
						selector: '.has-tip-other',
						additional_inheritable_classes: [],
						tooltip_class: '.tooltip_other',
						touch_close_text: '',
						disable_for_touch: true
					}
				});
			</script>
		<!-- End Main Scripts -->
	</body>
</html>